/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '$/jquery'

import ext from '$/io.ox/core/extensions'
import capabilities from '$/io.ox/core/capabilities'
import Tour from '$/io.ox/core/tk/wizard'
import ModalDialog from '$/io.ox/backbone/views/modal'
import multifactorAPI from '$/io.ox/multifactor/api'
import gt from 'gettext'
import { settings } from '@/io.ox/guidedtours/main'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'
import openSettings from '$/io.ox/settings/util'

const point = ext.point('io.ox/tours/multifactor')

let index = 100

function cancel () {
  $('.modal-dialog*:not([style*="display: none"])').find('.modal-footer button[data-action="cancel"]').click()
}

// Introduction point.  Show add button and choice box
point.extend({
  id: 'multifactor',
  index: index += 100,
  steps: function (baton) {
    if (!baton.tour || !capabilities.has('multifactor')) return

    baton.tour.step()
      .title(gt('Second Factor Authentication'))
      .waitFor('#addDevice')
      .on('wait', () => { openSettings('virtual/settings/security', 'io.ox/settings/security/multifactor') })
      .content(gt('You can now add additional verification options to enhance the security of your account.'))
      .referTo('#addDevice')
      .end()
    baton.tour.step()
      .title(gt('Adding verification option'))
      .waitFor('.modal[data-point*="addMultifactor"]')
      .on('wait', () => { $('#addDevice').click() })
      .content(gt('Various options for additional verification depends on your installation.'))
      .spotlight('.modal-body')
      .referTo('.modal-body')
      .on('show', () => {
        if ($('h1:contains("Removing")').closest('.wizard-step').length > 0) $('h1:contains("Removing")').closest('.wizard-step').remove()
      })
      .on('back', () => {
        if ($('.modal*:not([style*="display: none"])').length > 0) $('.modal*:not([style*="display: none"])').find('.modal-footer button[data-action="cancel"]').click()
        if ($('.modal-backdrop').length > 0) $('.modal-backdrop').remove()
      })
      .end()
  }
})

// Show SMS setup as an example (if enabled)
point.extend({
  id: 'multifactor_sms',
  index: index += 100,
  steps: function (baton) {
    if (!baton.tour || !capabilities.has('multifactor') || !baton.hasSMS) return
    // These steps only shown if SMS is enabled
    baton.tour.step()
      .title(gt('Adding a code via text message'))
      .waitFor('.select.countryCodes')
      .on('wait', () => {
        $('#deviceNumber').closest('.modal-dialog').show()
        if ($('#deviceNumber').length) return
        if ($('.modal-body .bi-phone.mfIcon').length === 0) {
          if ($('.modal-backdrop').length > 0) $('.modal-backdrop').remove()
          $('#addDevice').click()
        }
        $('.modal-body .bi-phone.mfIcon').click()
      })
      .on('show', () => { $('#deviceNumber').val('123456789') })
      .content(gt('Selecting the option for a code via text message, for example, will then ask you for a phone number.'))
      .on('next', function hideModal () { $('#deviceNumber').closest('.modal-dialog').hide() })
      .on('back', cancel)
      .spotlight('#deviceNumber', { padding: 5 })
      .end()
    baton.tour.step()
      .title(gt('Authentication Requests'))
      .waitFor('#verification')
      .on('wait', () => {
        import('$/io.ox/multifactor/views/smsProvider').then(function ({ default: viewer }) {
          const challengeData = {
            phoneNumberTail: '56789'
          }
          const authInfo = { device: { id: 'test', provider: 'test' }, def: $.Deferred() }
          viewer.open(challengeData, authInfo)
        })
      })
      .on('next', cancel)
      .on('back', cancel)
      .content(gt('Then the next time you log in, you will have to verify your identitity by entering the number sent to your phone.'))
      .spotlight('.modal-content')
      .end()
  }
})

// Done tour.  Mention 2fa can be removed once setup
point.extend({
  id: 'multifactor_done',
  index: index += 100,
  steps: function (baton) {
    if (!baton.tour || !capabilities.has('multifactor')) return
    baton.tour.step()
      .title(gt('Removing'))
      .on('before:show', () => { $('.modal.verification-options').hide() })
      .content(gt('Devices can be removed once added if you so choose.  See help for additional details.'))
      .end()
  }
})

Tour.registry.add({
  id: 'default/io.ox/multifactor',
  priority: 1
}, async () => {
  const tour = new Tour()

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', function stopTour () {
    $('.modal-backdrop.in').remove()
    $('div[data-point="multifactor/settings/addDevice/SMS"]').remove()
    $('div[data-point="multifactor/settings/addMultifactor"]').remove()
    $('div[data-point="multifactor/views/smsProvider"]').remove()
    $('[data-point="io.ox/core/settings/dialog"]').css('display', 'none')
    restoreFloatingWindows(openFloatingWindows)
  })

  const baton = new ext.Baton({ tour })
  baton.hasSMS = await multifactorAPI.getProviders().then(data => !!data.providers.find(provider => provider.name === 'SMS'))

  point.invoke('steps', this, baton)

  tour.steps[0].options.labelNext = gt('Next')

  new ModalDialog({ title: gt('Second Factor Authentication'), width: '600px' })
    .build(function () {
      // #. This tells the user in a welcome tour, that the product now offers Two-Factor-Authentication (2FA)
      this.$body.css('white-space', 'pre-line').append($('<div>').text(gt('As an additional security feature, you can now require a second method of authentication before logging into your account.  Would you like to see some information on this now?')))
    })
    .addCancelButton()
    .addButton({ action: 'start', label: gt('Start tour') })
    .on('start', () => {
      tour.start()
    })
    .open()
})

export default {
  run: function () {
    // Verify multifactor service running before starting tour
    if (!capabilities.has('multifactor') || !capabilities.has('multifactor_service')) return false
    // We are always on going to autoshow this tour only once
    settings.set('multifactor/shownTour', true).save()
    // Start tour
    Tour.registry.run('default/io.ox/multifactor')
    return true
  }
}
