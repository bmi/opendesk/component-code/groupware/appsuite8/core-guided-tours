/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ext from '$/io.ox/core/extensions'
import ox from '$/ox'
import $ from '$/jquery'

export function switchToAppFunc (name, yielded) {
  if (typeof yielded === 'undefined') {
    return function (yielded) {
      switchToApp(name, yielded)
    }
  }
  switchToApp(name, yielded)
}

export function switchToApp (name, yielded) {
  ox.load(() => import(name)).then(function ({ default: m }) {
    const app = m.getApp()
    app.launch().then(function () {
      const that = app
      if (name === 'io.ox/calendar/edit/main') {
        $(that).one('finishedCreating', yielded)// calendar needs some time to set up the view
        that.create({})
      } else if (name === 'io.ox/mail/write/main') {
        that.compose({ subject: '[Guided tours] Example email' })
        yielded()
      } else {
        yielded()
      }
    })
  })
}

export function tours () {
  const tours = {}
  ext.point('io.ox/tours/extensions').each(function (tourExtension) {
    const appId = tourExtension.app
    if (!tours[appId] || tourExtension.priority > tours[appId].priority) {
      tours[appId] = tourExtension.tour
      tours[appId].priority = tourExtension.priority
    }
  })
  return tours
}

export default {
  switchToAppFunc,
  switchToApp,
  tours,
  availableTours () {
    return Object.keys(tours())
  },
  isAvailable (tourname) {
    return Object.keys(tours()).indexOf(tourname) >= 0
  },
  get (tourname) {
    return tours()[tourname]
  }
}
