/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import Tour from '$/io.ox/core/tk/wizard'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'
import gt from 'gettext'

/* Tour: contacts / address book */
Tour.registry.add({
  id: 'default/io.ox/contacts',
  app: 'io.ox/contacts',
  priority: 1
}, function () {
  const tour = new Tour()

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', () => restoreFloatingWindows(openFloatingWindows))

  tour.step()
    .title(gt('Creating a new contact'))
    .content(gt('To create a new contact, click on New > Add contact in the toolbar.'))
    .spotlight('.primary-action', { position: 'left' })
    .hotspot('.primary-action button', { position: 'left' })
    .end()
    .step()
    .title(gt('Navigation bar'))
    .content(gt('Click on a letter on the left side of the navigation bar in order to display the corresponding contacts from the selected address book.'))
    .spotlight('.contact-grid-index')
    .end()
    .step()
    .title(gt('Sending an email to a contact'))
    .content(gt('To send an email to the contact, click on an email address or on Send email in the toolbar.'))
    .hotspot('.contact-detail [href^="mailto"]:first')
    .spotlight('.contact-detail [href^="mailto"]:first')
    .end()
    .step()
    .title(gt('Selecting multiple contacts'))
    .content(gt('To select multiple contacts, use the keys "shift" or "control" and click on the entries. Then you can export or copy them to other address books all at once.'))
    .spotlight('.vgrid-scrollpane')
    .end()
    .start()
})
