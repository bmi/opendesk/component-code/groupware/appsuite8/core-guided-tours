/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '$/jquery'

import registry from '$/io.ox/core/main/registry'
import Tour from '$/io.ox/core/tk/wizard'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'
import { openModals } from '$/io.ox/backbone/views/modal'

import gt from 'gettext'

let composeApp

Tour.registry.add({
  id: 'default/io.ox/mail',
  app: 'io.ox/mail',
  priority: 1
}, function () {
  const tour = new Tour()
  let prevState

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', () => restoreFloatingWindows(openFloatingWindows))

  tour.step()
    .title(gt('Composing a new email'))
    .content(gt('To compose a new email, click on Compose in the toolbar.'))
    .spotlight('.primary-action', { position: 'left' })
    .hotspot('.primary-action button', { position: 'left' })
    .on('next', function () {
      if (composeApp) {
        if (composeApp.getWindow().floating.model.get('minimized')) composeApp.getWindow().floating.model.set('minimized', false)
        return
      }
      registry.call('io.ox/mail/compose', 'open').then(function (result) {
        composeApp = result.app

        // if guard password prompt opens close it again
        openModals.queue.find(modal => modal?.point?.id === 'oxguard_core/auth').close()
      })
    })
    .end()
    .step()
    .title(gt('Entering the recipient\'s name'))
    .content(gt('Enter the recipient\'s name into the recipients field. As soon as you typed the first letters, suggestions from the address books are displayed. To accept a recipient suggestion, click on it.'))
    .waitFor('.io-ox-mail-compose-window:visible:last .subject:visible')
    .hotspot('.active [data-extension-id=to] .tokenfield.to', { top: 6 })
    .on('back', function () {
      if (composeApp && !composeApp.getWindow().floating.model.get('minimized')) {
        composeApp.getWindow().floating.onMinimize()
      }
    })
    .end()
    .step()
    .title(gt('Entering the subject'))
    .content(gt('Enter the subject into the subject field.'))
    .hotspot('.active [data-extension-id=subject] input', { top: 6 })
    .end()
    .step()
    .title(gt('Further functions'))
    .content(gt('In this area you can find further functions, e.g. for adding attachments.'))
    .hotspot('[data-extension-id=composetoolbar]', { top: 12, left: 12 })
    .spotlight('[data-extension-id="composetoolbar"]')
    .end()
    .step()
    .title(gt('Entering the email text'))
    .content(gt('Enter the email text into the main area. If the text format was set to HTML in the options, you can format the email text. To do so select a text part and then click an icon in the formatting bar.'))
    .referTo('.io-ox-mail-compose-container')
    .hotspot('.active .io-ox-mail-compose .editor', { top: 12, left: 12 })
    .spotlight('.active .io-ox-mail-compose .editor')
    .end()
    .step()
    .title(gt('Sending the email'))
    .content(gt('To send the email, click on Send'))
    .hotspot('.io-ox-mail-compose-window.active button[data-action=send]')
    .spotlight('.io-ox-mail-compose-window.active button[data-action=send]')
    .on('before:show', function () {
      if (composeApp && composeApp.getWindow().floating.model.get('minimized')) {
        composeApp.getWindow().floating.model.set('minimized', false)
      }
    })
    .end()
    .step()
    .on('before:show', function () {
      if (composeApp && !composeApp.getWindow().floating.model.get('minimized')) {
        composeApp.getWindow().floating.onMinimize()
      }
    })
    .title(gt('Sorting your emails'))
    // #. Sort is the label of a dropdown menu
    .content(gt('To sort the emails, click on Sort. Select a sort criteria.'))
    .waitFor('.io-ox-mail-window')
    .hotspot('.list-view-control .toolbar-item button')
    .spotlight('.list-view-control .toolbar-item button')
    .navigateTo(() => import('$/io.ox/mail/main'))
    .on('before:show', function () {
      prevState = $('.list-view-control .dropdown ul').get(0).style.display
      $('.list-view-control .dropdown ul').css('display', 'block')
    })
    .on('hide', function () { $('.list-view-control .dropdown ul').css('display', prevState) })
    .end()
    .step()
    .title(gt('Selecting a view'))
    .content(gt('To choose between the different views, click on View in the toolbar. Select a menu entry in the layout.'))
    .spotlight('#topbar-settings-dropdown div:has([data-name="layout"])', { position: 'left' })
    .hotspot('#io-ox-topbar-settings-dropdown-icon', { top: 22, left: 16 })
    .referTo('#topbar-settings-dropdown div:has([data-name="layout"])')
    .on('before:show', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', true) })
    .on('hide', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', false) })
    .end()
    .step()
    .title(gt('Opening an email in a separate window'))
    .content(gt('If double-clicking on an email in the list, the email is opened in a separate window.'))
    .spotlight('.list-view.mail-item > li.list-item:first')
    .end()
    .step()
    .title(gt('Reading email conversations'))
    // #. Sort is the label of a dropdown menu, keep this consistent
    .content(gt('If conversations are enabled in the Sort menu, you can collapse or expand an email in a conversation. To do so, click on a free area in the email headline.'))
    .hotspot('a[data-name="thread"]')
    .spotlight('a[data-name="thread"]')
    .on('before:show', function () {
      prevState = $('.list-view-control .dropdown ul').get(0).style.display
      $('.list-view-control .dropdown ul').css('display', 'block')
    })
    .on('hide', function () { $('.list-view-control .dropdown ul').css('display', prevState) })
    .end()

  if ($('.list-view-control .list-item.selectable').length > 0) {
    tour.step()
      .title(gt('Halo view'))
      .content(gt('To receive information about the sender or other recipients, open the Halo view by clicking on a name.'))
      .hotspot('a[data-detail-popup="halo"]')
      .spotlight('a[data-detail-popup="halo"]')
      .referTo('a[data-detail-popup="halo"]')
      .waitFor('a[data-detail-popup="halo"]')
      .on('before:show', function () {
        const read = $('.list-view.mail-item .list-item:not(.unread)').first()
        if ($('.mail-detail').length === 0) {
          read.length
            ? read.click()
            : $('.list-view.mail-item .list-item').first().click()
        }
      })
  }

  tour.step()
    .title(gt('Editing multiple emails'))
    .content(gt('In order to edit multiple emails at once, enable the checkboxes on the left side of the emails. If the checkboxes are not displayed, toggle Checkboxes in the settings dropdown of the toolbar.'))
    .hotspot('a[data-name="listViewLayout"][data-value="checkboxes"]')
    .spotlight('a[data-name="listViewLayout"][data-value="checkboxes"]')
    .referTo('a[data-name="listViewLayout"][data-value="checkboxes"]')
    .on('before:show', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', true) })
    .on('hide', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', false) })
    .end()
    .step()
    .title(gt('Opening the email settings'))
    .content(gt('To open the email settings, click the Settings menu icon on the upper right side of the menu bar. Click on Mail on the left side.'))
    .hotspot('#io-ox-topbar-settings-dropdown-icon', { top: 22, left: 16 })
    .on('before:show', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', true) })
    .on('hide', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', false) })
    .end()
    .on('stop', function () {
      if (composeApp) {
      // prevent app from asking about changed content
        composeApp.view.dirty(false)
        composeApp.quit()
        composeApp = null
      }
    })
    .start()
})
