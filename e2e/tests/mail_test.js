/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Client Onboarding > Tours')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Mail Tour', async ({ I, users }) => {
  await I.haveMail({
    attachments: [{
      content: 'Hello world!',
      content_type: 'text/html',
      disp: 'inline'
    }],
    from: [[users[0].get('display_name'), users[0].get('primaryEmail')]],
    sendtype: 0,
    subject: 'Hey!',
    to: [[users[0].get('display_name'), users[0].get('primaryEmail')]]
  })

  await I.login('app=io.ox/mail')

  I.click('~Help')
  I.click('Guided tour for this app')
  I.waitForElement('.wizard-container', 5)

  I.waitForText('Composing a new email')
  I.seeElement('.hotspot')
  I.click('Start tour')

  I.waitForText("Entering the recipient's name")
  I.seeElement('.hotspot')
  I.click('Next')

  I.waitForText('Entering the subject')
  I.seeElement('.hotspot')
  I.click('Next')

  I.waitForText('Further functions')
  I.click('Next')

  I.waitForText('Entering the email text')
  I.click('Next')

  I.waitForText('Sending the email')
  I.seeElement('.hotspot')
  I.click('Next')

  I.waitForText('Sorting your emails')
  I.seeElement('.hotspot')
  I.click('Next')

  I.waitForText('Selecting a view')
  I.click('Next')

  I.waitForText('Opening an email in a separate window')
  I.click('Next')

  I.waitForText('Reading email conversations')
  I.click('Next')

  I.waitForText('Halo view')
  I.click('Next')

  I.waitForText('Editing multiple emails')
  I.seeElement('.hotspot')
  I.click('Next')

  I.waitForText('Opening the email settings')
  I.click('Finish')

  // Check that wizard was closed
  I.waitForInvisible('.wizard-step', 5)
})
